﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFWebApplication
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IOrganizationService" в коде и файле конфигурации.
    [ServiceContract]
    public interface IOrganizationService
    {
        [OperationContract]
        void DoWork();

        [OperationContract]
        Organization GetOrganization(string orgId);

        [OperationContract]
        string GetOrganizationName(string orgId);

        [OperationContract]
        int GetOrganizationEmployeesCount(string orgId);

        [OperationContract]
        List<User> GetOrganizationEmployees(string orgId);
    }
}
