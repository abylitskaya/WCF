﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WCFWebApplication.Models
{
    public class UserModel
    {

        [Key]
        public string id { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        public string fullName { get; set; }

        [Display(Name = "Age")]
        public int age { get; set; }

        [Display(Name = "Role")]
        public virtual RoleModel role { get; set; }

        public virtual OrganizationModel organization { get; set; }
    }
}