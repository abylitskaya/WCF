﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WCFWebApplication.Models
{
    public class RoleModel
    {
        [Key]
        public string id { get; set; }

        public string name { get; set; }
    }
}