﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WCFWebApplication.Models
{
    public class OrganizationModel
    {
        [Key]
        public string id { get; set; }

        public string name { get; set; }

        public virtual List<UserModel> employees { get; set; }
    }
}