﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFWebApplication
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IUserService" в коде и файле конфигурации.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        void AddUser(User user);

        [OperationContract]
        User GetUser(string userId);

        [OperationContract]
        void SetUser(User newUser);

        [OperationContract]
        void RemoveUser(string userId);

        // Get all users

        [OperationContract]
        List<User> GetUsers();

        // get property methods

        [OperationContract]
        string GetName(string userId);

        [OperationContract]
        string GetLastName(string userId);

        [OperationContract]
        string GetFullName(string userId);

        [OperationContract]
        int GetAge(string userId);

        [OperationContract]
        string GetRoleName(string userId);

        // set properties methods

        [OperationContract]
        void SetName(string userId, string name);

        [OperationContract]
        void SetLastName(string userId, string lastName);

        [OperationContract]
        void SetAge(string userId, int age);

        [OperationContract]
        void SetRole(string userId, string roleId);
    }
}
