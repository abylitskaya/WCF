﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WCFWebApplication.Startup))]
namespace WCFWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
