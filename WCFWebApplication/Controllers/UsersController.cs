﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCFWebApplication.UserServiceReference;

namespace WCFWebApplication.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            using (var client = new UserServiceClient())
            {
                UserServiceReference.User[] users = client.GetUsers();
                ViewBag.users = users;
            }
            return View();
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            return View();
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                UserServiceReference.User user = new UserServiceReference.User();
                user.name = collection["name"].ToString();
                user.lastName = collection["lastName"].ToString();
                user.age = Int32.Parse(collection["age"].ToString());
                user.roleId = "3";

                using (var client = new UserServiceClient())
                {
                    client.AddUser(user);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            using (var client = new UserServiceClient())
            {
                UserServiceReference.User user = client.GetUser(id);
                ViewBag.user = user;
            }
            return View();
        }

        // POST: Users/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, FormCollection collection)
        {
            try
            {
                using (var client = new UserServiceClient())
                {
                    UserServiceReference.User user = client.GetUser(id);
                    user.name = collection["name"].ToString();
                    user.lastName = collection["lastName"].ToString();
                    user.age = Int32.Parse(collection["age"].ToString());

                    client.SetUser(user);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            return View();
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                using (var client = new UserServiceClient())
                {
                    UserServiceReference.User user = client.GetUser(id.ToString());

                    client.RemoveUser(id.ToString());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
