﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFWebApplication
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "UserService" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы UserService.svc или UserService.svc.cs в обозревателе решений и начните отладку.
    public class UserService : IUserService
    {

        // CRUD

        public void AddUser(User user)
        {
            List<User> users = jsonReader.LoadUsersJson();
            users.Add(user);
            jsonReader.WriteUsersJson(users);
        }

        public User GetUser(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) return user;
            }
            return new User();
        }

        public void SetUser(User newUser)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == newUser.id)
                {
                    user.name = newUser.name;
                    user.lastName = newUser.lastName;
                    user.age = newUser.age;
                    user.roleId = newUser.roleId;
                }
            }
            jsonReader.WriteUsersJson(users);
        }

        public void RemoveUser(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) users.Remove(user);
            }
        }

        // Get all users

        public List<User> GetUsers()
        {
            List<User> users = jsonReader.LoadUsersJson();
            return users;
        }

        // get property methods

        public string GetName(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) return user.name;
            }
            return "";
        }

        public string GetLastName(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) return user.lastName;
            }
            return "";
        }

        public string GetFullName(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) return user.name + " " + user.lastName;
            }
            return "";
        }

        public int GetAge(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) return user.age;
            }
            return -1;
        }

        public string GetRoleName(string userId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            List<Role> roles = jsonReader.LoadRolesJson();

            foreach (var user in users)
            {
                if (user.id == userId) return user.roleId;
            }
            return "";
        }

        // set properties methods

        public void SetName(string userId, string name)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) user.name = name;
            }
            jsonReader.WriteUsersJson(users);
        }

        public void SetLastName(string userId, string lastName)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) user.lastName = lastName;
            }
            jsonReader.WriteUsersJson(users);
        }

        public void SetAge(string userId, int age)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) user.age = age;
            }
            jsonReader.WriteUsersJson(users);
        }

        public void SetRole(string userId, string roleId)
        {
            List<User> users = jsonReader.LoadUsersJson();
            foreach (var user in users)
            {
                if (user.id == userId) user.roleId = roleId;
            }
            jsonReader.WriteUsersJson(users);
        }
    }
}
