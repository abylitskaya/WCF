﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace WCFWebApplication
{
    public class jsonReader
    {
        // Organizations
        public static List<Organization> LoadOrganizationsJson()
        {
            using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~") + "/data/organizations.json"))
            {
                string json = r.ReadToEnd();

                List<Organization> organizations = JsonConvert.DeserializeObject<List<Organization>>(json);
                return organizations;
            }
        }

        public static void WriteOrganizationsJson(List<Organization> data)
        {
            string json = JsonConvert.SerializeObject(data.ToArray());
            File.WriteAllText(HttpContext.Current.Server.MapPath("~") + "/data/organizations.json", json);
        }

        // Users
        public static List<User> LoadUsersJson()
        {
            using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~") + "/data/users.json"))
            {
                string json = r.ReadToEnd();
                List<User> users = JsonConvert.DeserializeObject<List<User>>(json);
                return users;
            }
        }

        public static void WriteUsersJson(List<User> data)
        {
            string json = JsonConvert.SerializeObject(data.ToArray());
            File.WriteAllText(HttpContext.Current.Server.MapPath("~") + "/data/users.json", json);
        }

        // Roles
        public static List<Role> LoadRolesJson()
        {
            using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~") + "/data/roles.json"))
            {
                string json = r.ReadToEnd();
                List<Role> roles = JsonConvert.DeserializeObject<List<Role>>(json);
                return roles;
            }
        }

        public static void WriteRoleJson(List<Role> data)
        {
            string json = JsonConvert.SerializeObject(data.ToArray());
            File.WriteAllText(HttpContext.Current.Server.MapPath("~") + "/data/roles.json", json);
        }

    }
}