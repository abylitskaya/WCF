﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFWebApplication
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "OrganizationService" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы OrganizationService.svc или OrganizationService.svc.cs в обозревателе решений и начните отладку.
    public class OrganizationService : IOrganizationService
    {
        public void DoWork()
        {
        }

        public Organization GetOrganization(string orgId)
        {
            return new Organization("HiQo");
        }

        public string GetOrganizationName(string orgId)
        {
            return "HiQo";
        }

        public int GetOrganizationEmployeesCount(string orgId)
        {
            return 666;
        }

        public List<User> GetOrganizationEmployees(string orgId)
        {
            return new List<User>(3);
        }
    }
}
