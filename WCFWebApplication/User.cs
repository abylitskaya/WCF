﻿using System;

namespace WCFWebApplication
{
    [Serializable]
    public class User
    {
        public string id;
        public string name;
        public string lastName;
        public string fullName;
        public string roleId;
        public int age;

        public static int usersCount = 0;

        public User()
        {
            this.name = "Ilya";
            this.lastName = "Vrublevsky";
            this.fullName = "Ilya Vrublevsky";
            this.age = 23;
            this.roleId = "3";
            this.id = (User.usersCount++).ToString();
        }

        public User(string name, string lastName, int age, string roleId)
        {
            this.name = name;
            this.lastName = lastName;
            this.fullName = name + " " + lastName;
            this.age = age;
            this.roleId = roleId;
            this.id = (User.usersCount++).ToString();
        }
    }
}