﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services
{
    [Serializable]
    public class Organization
    {
        public string id;
        public string name;
        public List<User> employees;

        public static int organizationsCount = 0;

        public Organization(string name)
        {
            this.name = name;
            this.employees = new List<User>();
            this.id = (Organization.organizationsCount++).ToString();
        }
    }