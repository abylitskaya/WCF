﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services
{
    public class Role
    {
        public string id;
        public string name;

        public static int rolesCount = 0;

        public Role(string name)
        {
            this.name = name;
            this.id = (Role.rolesCount++).ToString();
        }
    }
}